import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
// import { WebBrowser } from 'expo';
import Icon from 'react-native-vector-icons/Ionicons';
import { RkTextInput, RkCard } from 'react-native-ui-kitten';
import { MonoText } from '../components/StyledText';

export default class MediHome extends React.Component {
  static navigationOptions = {
    header: null,
  };

  DiseaseCard() {
    return (
      <RkCard>
        <View rkCardHeader>
          <Text>Header</Text>
        </View>
        <Image rkCardImg source={{uri:'http://via.placeholder.com/140x140'}}/>
        <View rkCardContent>
          <Text> quick brown fox jumps over the lazy dog</Text>
        </View>
        <View rkCardFooter>
          <Text>Footer</Text>
        </View>
    </RkCard>
  );
  }
  //
  render() {
    return (
      <View style={{backgroundColor: '#fff',flex:1}}>
        <View style={styles.container}>
          <RkTextInput label={<Icon name={'ios-search'}/>}/>
          <ScrollView>
            {this.DiseaseCard()}
            {this.DiseaseCard()}
          </ScrollView>  
        </View>

      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal:32,


  },
});
